import React from 'react';
import '../styles/Solution.css'


const Solution = () => {
  return (
    <div className = "solution">
        <div className='topHalf'>
            <h2>How it works</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis elementum dui, 
                non vehicula diam maximus quis. Cras commodo eros eget tellus rutrum congue et sed mi. 
                Duis pulvinar neque et hendrerit vehicula. Praesent ante urna, aliquet vitae semper sit amet,
                suscipit vestibulum mauris. Quisque sit amet mi sit amet mauris suscipit suscipit. Vestibulum ante 
                ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus rutrum sed lorem
                sit amet scelerisque. In in laoreet nibh, vel pellentesque risus. Etiam gravida elit sit amet orci malesuada,
                in posuere eros efficitur. Aenean iaculis massa eget lectus lacinia, et porta nulla lobortis. Aliquam 
                hendrerit iaculis malesuada. Etiam erat ligula, lacinia at blandit congue, euismod a tellus.</p>
        </div>

        <div className='bottomHalf'>
        
        </div>
    </div>

    
  )
}

export default Solution