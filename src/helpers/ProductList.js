import Barrel4 from '../assets/pics/4_Barrel.jpg';
import Barrel9 from '../assets/pics/9_Barrel.jpg';
import Block0A from '../assets/pics/Block_0A.jpg';
import DroneCounterMeasure2 from '../assets/pics/Drone Countermeasure 2.png';


export const ProductList =[
    {
        name: "Barrel4",
        image: Barrel4,
        price: 10000
    },
    {
        name: "Barrel9",
        image: Barrel9,
        price: 12000
    },
    {
        name: "Block_0A",
        image: Block0A,
        price: 16000
    },
    {
        name: "Drone Counter Measure",
        image: DroneCounterMeasure2,
        price: 20000
    },

]